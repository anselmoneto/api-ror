# Rails.application.routes.draw do
#   devise_for :users
#   # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
# end
	
Rails.application.routes.draw do
  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :contacts
    end
  end
  # O exemplo acima ficaria assim: www.dominio.com/api/v1/contacts
 
  # constraints subdomain: 'api' do
  #   scope module: 'api' do
  #     namespace :v1 do
  #       resources :contacts
  #     end
  #   end
  # end
  # O exemplo acima seria para usar um subdominio --- ex: api.dominio.com/v1/contacts
end